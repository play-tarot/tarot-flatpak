#!/bin/bash

GPG_SIGN=""

if gpg --import /tarot.gpg
then
    GPG_SIGN="--gpg-sign=C8ECB5A2"
fi

LATEST_ARTIFACT_URL="$CI_PROJECT_URL/-/jobs/artifacts/master/download?job=pages"

echo "Downloading the previous repo from $LATEST_ARTIFACT_URL"

curl -o artifacts.zip "$LATEST_ARTIFACT_URL" || echo "WARNING: NO ARTIFACTS!"

unzip artifacts.zip || mkdir public

flatpak-builder --repo=public "$GPG_SIGN" build-dir eu.planete_kraus.Tarot.json || exit 1
(echo "pass" ; echo "pass") | flatpak-builder --run build-dir eu.planete_kraus.Tarot.json tarot -s ";)" || exit 1

cp eu.planete_kraus.Tarot.json public/
mkdir -p public/other
cp other-eu.planete_kraus.Tarot.json public/other/eu.planete_kraus.Tarot.json

if test "x$CI_PAGES_URL" = "x"
then
    export CI_PAGES_URL="https://play-tarot.frama.io/tarot-flatpak"
fi

curl -o public/tarot.svg "$CI_PAGES_URL/../tarot/share/doc/tarot/tarot.svg"

cat > public/tarot.flatpakrepo <<EOF
[Flatpak Repo]
Title = Tarot repository
Url = $CI_PAGES_URL
Homepage = $CI_PAGES_URL/../tarot/share/doc/tarot/tarot.html/index.html
Comment = The Amazing Repository Of TAROT
Description = This is where I host the flatpaks for my personal project.
Icon = $CI_PAGES_URL/tarot.svg
GPGKey = $(gpg --export C8ECB5A2 | base64 --wrap=0)
EOF
