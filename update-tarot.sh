#!/bin/bash

for file in update-tarot.sh update-libtarot.sh eu.planete_kraus.Tarot.json flathub/eu.planete_kraus.Tarot.json
do
    sed -i 's|https://framagit.org/play-tarot/tarot/uploads/6395ef908d58678606e79fde82eb71d1/tarot-1.1.1.tar.gz|'"$1"'|g' $file
    sed -i 's|b6658cb7a1f1578fe592e4bf8bbca6afd27dd64a31248722f9ff0dd8b1aeef8f|'"$2"'|g' $file
done
