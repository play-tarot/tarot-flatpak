#!/bin/bash

for file in update-libtarot.sh update-libtarot.sh eu.planete_kraus.Tarot.json flathub/eu.planete_kraus.Tarot.json
do
    sed -i 's|https://framagit.org/play-tarot/libtarot/uploads/247b8da35b2371d964322a4dc97b2949/libtarot-0.6.0.tar.gz|'"$1"'|g' $file
    sed -i 's|b5566b25f3e683975bf995205703d2c61a3e49cac898e5db75fa2bf06bd54884|'"$2"'|g' $file
done
